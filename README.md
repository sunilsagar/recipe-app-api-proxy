# Recipe App API Proxy

NGINX Prosy app for our recipe API 

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)